#Design Principles

*Single Responsibility Principle (SRP)
*Tell Don't Ask (TDA)
*Encapsulate what varies
*Code to contracts not implementations
*Open Close Principle (OCP)
*Favour composition over inheritance
*Don't Repeat Yourself (DRY)


#Patterns

*Specification
*Composite
*Adapter
*Null Object
